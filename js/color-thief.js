(function (Drupal) {
  /**
   * Remove the placeholder background color so it doesn't interfere with images that have transparency.
   */
  Drupal.behaviors.colorThiefRemovePlaceholderBackground = {
    attach(context) {
      var imagesWithPlaceholders = context.querySelectorAll(
        "img[data-color-thief], picture[data-color-thief] img",
      );

      function removePlaceholderBackground(el) {
        if (el.parentElement.nodeName === 'PICTURE') {
          removePlaceholderBackground(el.parentElement);
        } else {
          el.style.removeProperty("background-color");
          if (el.getAttribute("style") === "") {
            el.removeAttribute("style");
          }
        }
      }

      imagesWithPlaceholders.forEach(function (image) {
        image.addEventListener("load", function () {
          removePlaceholderBackground(this);
        });

        if (image.complete) {
          removePlaceholderBackground(image);
        }
      });
    },
  };
})(Drupal);
