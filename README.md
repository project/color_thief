## INTRODUCTION

The Color Thief module uses a PHP port of the
[Color Thief library](https://github.com/ksubileau/color-thief-php) to extract
the dominant color from an image file entity so that it can be used as a
placeholder while the image loads.

The module provides two field formatters:

1. _Image with dominant color background_
2. _Responsive image with dominant color background_

The dominant color is set as a CSS background color on the `img` element when
one of the field formatters provided by this module is used.

If space is reserved for the image then the background color will be visible
while the image loads, but it will be obscured once the image has fully loaded.
A small amount of JavaScript is used to remove the background color once each
image has loaded to prevent any issues with transparent images where the
dominant color background could show through any alpha channels.

This module does not provide any lazyloading capabilities and it does not ensure
that space is reserved for images as they load. For non-responsive images,
Drupal adds a `width` and `height` HTML attribute to the image, which should
reserve space in modern browsers. For responsive images, space can be reserved
via a [CSS technique](https://css-tricks.com/aspect-ratio-boxes) until the
Drupal Core issue
[#3192234](https://www.drupal.org/project/drupal/issues/3192234) is resolved.

## REQUIREMENTS

Composer is required to install
[`ksubileau/color-thief-php`](https://packagist.org/packages/ksubileau/color-thief-php).

The `thumbnail` image style provided by the Image module must be available.

## INSTALLATION

Install the Color Thief module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

## CONFIGURATION

    1. Navigate to Administration > Extend and enable the Color Thief module.
    2. Navigate to the 'Manage display' tab of a entity with an Image field
    3. Select the 'Image with dominant color background' or 'Responsive image
       with dominant color background' field formatter

Newly uploaded image files will have their dominant color calculated and used as
a placeholder immediately.

The dominant color of existing files will be calculated in the background. When an
existing image file is displayed using one of the field formatters provided by
this module it will be added to a queue that runs as part of Drupal's cron
tasks. When cron runs, Drupal will work through the queue of image files and
calculate the dominant color for each one. Once the color has been calculated it
will be used as a placeholder the next time that image file is displayed using
one of the Color Thief field formatters.

## MAINTAINERS

- Phil Wolstenholme - https://www.drupal.org/u/phil-wolstenholme
