<?php

namespace Drupal\color_thief;

use Drupal\file\FileInterface;

/**
 * Trait to share utility methods between Color Thief field formatters.
 */
trait ColorThiefTrait {

  /**
   * Adds a file to the queue of files to have their color calculated on cron.
   *
   * @param \Drupal\file\FileInterface $file
   *   A file entity.
   */
  private function addFileEntityToColorThiefQueue(FileInterface $file) {
    /** @var \Drupal\Core\Queue\QueueInterface $queue */
    $queue = $this->queueFactory->get('color_thief_color_setter');
    $item = new \stdClass();
    $item->fid = $file->id();
    $queue->createItem($item);
  }

  /**
   * Checks the database to see if we have calculated this file's color already.
   *
   * @param \Drupal\file\FileInterface $file
   *   A file entity.
   *
   * @return string|false
   *   Either a dominant colour or false.
   */
  private function getColorFromDatabase(FileInterface $file) {
    // Try and load from the DB first.
    /** @var \Drupal\Core\Database\Connection $connection */
    $query = $this->database->select('color_thief', 'ct');
    $query->addField('ct', 'color');
    $query->condition('ct.fid', $file->id());
    $results = $query->execute();
    $color = $results->fetchCol();

    if ($color) {
      return $color[0];
    }

    return FALSE;
  }

  /**
   * Alters a render array to provide a background color placeholder.
   *
   * @param array $element
   *   A render element.
   * @param string $dominant_color
   *   The dominant colour as a hex code without the hash symbol.
   */
  private function prepareImageElement(array &$element, string $dominant_color) {
    $background_css = "background-color:#$dominant_color;";
    $style_attribute = &$element['#item_attributes']['style'];

    if (!empty($style_attribute)) {
      $style_attribute .= $background_css;
    }
    else {
      $style_attribute = $background_css;
    }

    $element['#item_attributes']['data-color-thief'] = '';
    $element['#attached']['library'][] = 'color_thief/color_thief';
  }

}
